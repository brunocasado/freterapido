import unittest
from freterapido.client import Client
from freterapido.volume import Volume
from freterapido.nfe import NFe


class TestClient(unittest.TestCase):

    def test_cal_freight(self):

        volume = Volume('1', 'sku001', 2, 5, 5, 5, 5, 200.00)

        response = Client().calc_freight('30411872000103', '1', 'cpf', 'cep', [
            volume, ], '1', '305405347', 'token')

        # print(response.content)
        print(response.json())

        self.assertEqual(200, response.status_code)

    def test_hire_freight(self):

        response = Client().hire_freight(token_offer='136599c3940a5219',
                                         offer='198295', token='xxxxxxx',
                                         sender_cnpj='30411872000103',
                                         receiver_cnpj_cpf='xxxxxxx', receiver_name='Carlos Eduardo',
                                         receiver_email='xxxxxxxx', receiver_phone='11999999999',
                                         receiver_cep='xxxxxxxx', receiver_street='An Street',
                                         receiver_number='999', receiver_district='District', receiver_compl='Compl',
                                         order_number='1234')

        print(response.json())
        self.assertEqual(200, response.status_code)

    def test_send_nfe(self):

        nfe = NFe(number='12345778', serie='1234',
                  access_key='12345', value=250.00)

        response = Client().send_nfe(
            '#TESTE123', '2a34dbb0db2403463b46deb0e8511003', [nfe, ])
        print(response.content)
        print(response.json())
        self.assertEqual(200, response.status_code)
