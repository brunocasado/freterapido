from freterapido.base import Base
from freterapido.dispatcher import Dispatcher
from freterapido.receiver import Receiver
from freterapido.sender import Sender
from freterapido.volume import Volume
from freterapido.nfe import NFe
import requests
import json


class Client():

    API_URL_BASE = 'https://freterapido.com/api/external/embarcador/v1'

    def __init__(self, debug=False):
        if debug:
            self.API_URL_BASE = 'https://freterapido.com/sandbox/api/external/embarcador/v1'

    FREIGHT_FRACTIONED = 1

    FREIGHT_TYPES = (
        FREIGHT_FRACTIONED,
    )

    def calc_freight(self, sender_cnpj_cpf, receiver_type_person,
                     receiver_cnpj_cpf,
                     receiver_cep, volumes, freight_type, platform_code,
                     token, dispatcher_cep=''):
        ''' Method for get freight values

            Documentation: https://freterapido.com/dev/dist/api-ecommerce.html#/!#content_simulacao
            for parameter explain see freterapido docs

            PS: volumes is an list of volume instance

            Keyword arguments:
            all - see documentation
            volumes -- list of volume instance

        '''

        sender = Sender(cnpj=sender_cnpj_cpf)
        receiver = Receiver(tipo_pessoa=receiver_type_person, cnpj_cpf=receiver_cnpj_cpf,
                            cep=receiver_cep)

        if int(freight_type) not in self.FREIGHT_TYPES:
            raise ValueError('Freight type not allowed')

        request_data = {
            'remetente': sender,
            'destinatario': receiver,
            'tipo_frete': freight_type,
            'codigo_plataforma': platform_code,
            'token': token,
            'volumes': []
        }

        for volume in volumes:
            request_data['volumes'].append(volume)

        json_data = json.dumps(request_data, cls=Base.CustomEncoder, indent=4)

        return self._send_request('POST', '/quote-simulator',
                                  json_data)

    def hire_freight(self, token_offer, offer, token,
                     sender_cnpj,
                     receiver_cnpj_cpf, receiver_name,
                     receiver_email, receiver_phone, receiver_cep,
                     receiver_street, receiver_number, receiver_district,
                     receiver_compl='', order_number='', receiver_ie=''):

        sender = Sender(cnpj=sender_cnpj)
        receiver = Receiver(tipo_pessoa=1, cnpj_cpf=receiver_cnpj_cpf, ie=receiver_ie,
                            nome=receiver_name, email=receiver_email, telefone=receiver_phone,
                            cep=receiver_cep, rua=receiver_street, numero=receiver_number,
                            bairro=receiver_district, complemento=receiver_compl)

        request_data = {
            'remetente': sender,
            'destinatario': receiver,
            'numero_pedido': order_number,
        }

        json_data = json.dumps(request_data, cls=Base.CustomEncoder, indent=4)

        verb = '/quote/ecommerce/%s/offer/%s?token=%s' % (
            token_offer, offer, token)

        return self._send_request('POST', verb, json_data)

    def send_nfe(self, id_freight, token, NFes):
        import urllib
        request_data = {
            'nota_fiscal': []
        }

        for nfe in NFes:
            request_data['nota_fiscal'].append(nfe)

        json_data = json.dumps(request_data, cls=Base.CustomEncoder, indent=4)

        verb = '/quotes/%s/invoices?token=%s' % (
            urllib.parse.quote(id_freight), token)

        return self._send_request('POST', verb, json_data)

    def _send_request(self, method, verb, json_data):
        url = self.API_URL_BASE + verb
        # print(url)
        if method == 'GET':
            return requests.get(url, data=json_data, headers={'Content-Type': 'application/json'})
        elif method == 'POST':
            return requests.post(url, data=json_data, headers={'Content-Type': 'application/json'})
        else:
            raise Exception('method not allowed')
