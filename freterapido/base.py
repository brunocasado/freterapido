import json


class Base():

    class CustomEncoder(json.JSONEncoder):
        def default(self, obj):
            return {k.lstrip('_'): v for k, v in vars(obj).items()}

    def toJSON(self):
        return json.dumps(self, sort_keys=True, indent=4, cls=self.CustomEncoder)
