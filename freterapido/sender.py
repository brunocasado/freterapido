''' Class sender  '''
from freterapido.base import Base


class Sender(Base):
    def __init__(self, cnpj=''):
        self._cnpj = cnpj

    def _getCNPJ(self):
        return self._cnpj

    def _setCNPJ(self, v):
        self._cnpj = v

    cnpj = property(_getCNPJ, _setCNPJ)
