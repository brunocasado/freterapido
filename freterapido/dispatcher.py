from freterapido.base import Base
class Dispatcher(Base):
    def __init__(self, cep):
        self.endereco = {
            'cep': cep
        }

    def _getCEP(self):
        return self.endereco['cep']

    def _setCEP(self, v):
        self.endereco['cep'] = v

    cep = property(_getCEP, _setCEP)

