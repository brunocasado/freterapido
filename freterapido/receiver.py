from freterapido.base import Base


class Receiver(Base):

    TIPO_FISICO = 1
    ALLOWED_TIPO = (TIPO_FISICO)

    def __init__(self, tipo_pessoa, cnpj_cpf, cep, ie='', rua='', numero='', bairro='', complemento='', nome='', email='', telefone=''):

        self._tipo_pessoa = tipo_pessoa
        self._cnpj_cpf = cnpj_cpf
        self.nome = nome
        self.email = email
        self.telefone = telefone

        self._endereco = {
            'cep': cep
        }

        if rua:
            self._endereco['rua'] = rua

        if numero:
            self._endereco['numero'] = numero

        if bairro:
            self._endereco['bairro'] = bairro

        if complemento:
            self._endereco['complemento'] = complemento

        if ie:
            self._inscricao_estadual = ie

    def _set_tipo_pessoa(self, v):
        if not v in self.ALLOWED_TIPO:
            raise ValueError('type not allowed')

        self._tipo_pessoa = v

    def _get_tipo_pessoa(self):
        return self._tipo_pessoa

    def _get_cnpj_cpf(self):
        return self._cnpj_cpf

    def _get_rua(self):
        return self._endereco['rua'] if 'rua' in self._endereco['rua'] else ''

    def _get_numero(self):
        return self._endereco['numero'] if 'numero' in self._endereco['numero'] else ''

    def _get_bairro(self):
        return self._endereco['bairro'] if 'bairro' in self._endereco['bairro'] else ''

    def _get_complemento(self):
        return self._endereco['complemento'] if 'complemento' in self._endereco['complemento'] else ''

    def _get_cep(self):
        return self._endereco['cep']

    def _get_inscricao_estadual(self):
        return self._inscricao_estadual

    def _set_cnpj_cpf(self, v):
        self._cnpj_cpf = v

    def _set_inscricao_estadual(self, v):
        self._inscricao_estadual = v

    def _set_cep(self, v):
        self._endereco['cep'] = v

    def _set_rua(self, v):
        self._endereco['rua'] = v

    def _set_numero(self, v):
        self._endereco['numero'] = v

    def _set_bairro(self, v):
        self._endereco['bairro'] = v

    def _set_complemento(self, v):
        self._endereco['complemento'] = v

    tipo_pessoa = property(_get_tipo_pessoa, _set_tipo_pessoa)
    cnpj_cpf = property(_get_cnpj_cpf, _set_cnpj_cpf)
    inscricao_estadual = property(
        _get_inscricao_estadual, _set_inscricao_estadual
    )
    cep = property(_get_cep, _set_cep)
