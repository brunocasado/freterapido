from freterapido.base import Base

class Volume(Base):

    def __init__(self, tipo, sku, quantidade, altura, largura, comprimento, peso, valor):
        self._tipo = tipo
        self.sku = sku
        self._quantidade = int(quantidade)
        self._altura = float(altura)
        self._largura = float(largura)
        self._comprimento = float(comprimento)
        self._peso = float(peso)
        self._valor = float(valor)

    def _set_tipo(self, tipo):
        # if not tipo in self.TIPOS:
        #     raise Exception('Type not allowed')
        self._tipo = tipo

    def _set_quantidade(self, v):
        self._quantidade = int(v)

    def _set_altura(self, v):
        self._altura = float(v)

    def _set_largura(self, v):
        self._largura = float(v)

    def _set_comprimento(self, v):
        self._comprimento = float(v)

    def _set_peso(self, v):
        self._peso = float(v)

    def _set_valor(self, v):
        self._valor = float(v)

    def _get_tipo(self):
        return self._tipo

    def _get_quantidade(self):
        return self._quantidade

    def _get_altura(self):
        return self._altura

    def _get_largura(self):
        return self._largura

    def _get_comprimento(self):
        return self._comprimento

    def _get_peso(self):
        return self._peso

    def _get_valor(self):
        return self._valor

    tipo = property(_get_tipo, _set_tipo)
    quantidade = property(_get_quantidade, _set_quantidade)
    altura = property(_get_altura, _set_altura)
    largura = property(_get_largura, _set_largura)
    comprimento = property(_get_comprimento, _set_comprimento)
    peso = property(_get_peso, _set_peso)
    valor = property(_get_valor, _set_valor)
