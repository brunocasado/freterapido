from setuptools import setup

setup(name='freterapido',
      version='0.1',
      description='Frete rapido lib integration ',
      url='',
      author='Bruno Casado',
      author_email='contato@brunocasado.me',
      license='MIT',
      packages=['freterapido'],
      install_requires=['jsonpickle'],
      zip_safe=False)
